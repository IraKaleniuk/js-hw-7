"use strict";

function filterBy(arr,type) {
    const types = ["string", "number", "boolean", "object", "undefined"];
    return !types.includes(type)
    ? `Error! Enter one of types: ${types.join(", ")}`
        : arr.filter(el =>  typeof el !== type);
}

console.log(filterBy(["str", false, 1, "str1", undefined, 2,
    "str2", [3, 4, 5], NaN, null, undefined, true ], "string"));

